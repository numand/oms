$ontext
Konno-Yamazaki'nin Ortalama Mutlak Sapma Modeli için GAMS Modeli

Copyright (C) 2015-2018 Numan Demirdöğen <if.gnu.linux@gmail.com>

LİSANS:
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

$offtext

Sets
  j 'varlıklar'
  t 'süre'
;

Parameters
  a(j,t) 'düzeltilmiş ortalama getiri'
  r(j) 'ortalama getiri'
;

$gdxin portfolio.gdx
$load j t
$loaddc a r

Scalar
   M0 'fon' /1/
   ro 'en küçük getiri oranı' /0.01/
;

Variables
   x(j) 'yatırım miktarı'
   y(t) 'yardımcı değişken'
   z
;

positive variable x;

equations
   amac
   kisit1(t)
   kisit2(t)
   getirikisiti
   fonkisiti
;

amac..      z =e= sum(t, y(t))/card(t);
kisit1(t)..  y(t) + sum(j,a(j,t)*x(j)) =g= 0;
kisit2(t)..  y(t) - sum(j,a(j,t)*x(j)) =g= 0;
getirikisiti..   sum(i, r(j)*x(j)) =g= ro*M0;
fonkisiti..   sum(i, x(j)) =e= M0;

model m1 /amac,kisit1,kisit2,getirikisiti,fonkisiti/;
solve m1 using lp minimizing z;
